package com.digitalnatives.whosout;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class SignUpActivity extends Activity {
//test commit comment
    Button btnRegister;
    EditText userSignEditText;
    EditText phoneSignEditText;
    EditText emailSignEditText;
    EditText passSignEditText;
    EditText confirmSignEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        //remove intent launch animation
        overridePendingTransition(0, 0);

        //set register onClick Listener
        Button btnRegister = (Button)findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Register method launch code here
                //
                //
                //Launch MainActivity intent
                Intent intentMainActivity = new Intent(v.getContext(), MainActivity.class);
                startActivity(intentMainActivity);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //remove activity animation on back button
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
